<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'home', 'uses' => 'Frontend\HomeController@index' ]);
Route::get('/nhom-bai-viet/{slug}', ['as' => 'article_category', 'uses' => 'Frontend\ArticleController@category' ]);
Route::get('/bai-viet/{slug}', ['as' => 'article_detail', 'uses' => 'Frontend\ArticleController@detail' ]);
Route::get('/nhom-san-pham/{slug}', ['as' => 'product_category', 'uses' => 'Frontend\ProductController@category' ]);
Route::get('/san-pham/{slug}', ['as' => 'product_detail', 'uses' => 'Frontend\ProductController@detail' ]);
