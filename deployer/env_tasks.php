<?php

namespace Deployer;


set('shared_files', []);

task( 'env:upload', function(){
    upload( __DIR__ . '/' . get( 'env_file'), "{{release_path}}/.env");
});
task( 'env:update', function(){
    upload( __DIR__ . '/' . get( 'env_file'), "{{current_path}}/.env");
});