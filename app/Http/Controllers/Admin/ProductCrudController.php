<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProductRequest;
use App\Models\ProductCategory;
use App\Models\Tag;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ProductCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ProductCrudController extends CrudController {
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\BulkCloneOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\BulkDeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\FetchOperation;
    
    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup() {
        CRUD::setModel( \App\Models\Product::class );
        CRUD::setRoute( config( 'backpack.base.route_prefix' ) . '/product' );
        CRUD::setEntityNameStrings( 'sản phẩm', 'sản phẩm' );
        CRUD::addClause( 'with', 'product_category' );
    }
    
    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation() {
        CRUD::addColumns( [
            [
                'name' => 'name',
                'type' => 'text',
            ],
            [
                'name' => 'code',
                'type' => 'text',
            ],
            [
                'name' => 'price',
                'type' => 'number',
                'prefix' => 'VNĐ',
            ],
            [
                'name' => 'product_category_id',
                'type' => 'closure',
                'function' => function($entry){
                    return $entry->product_category ? $entry->product_category->name : $entry->product_category_id;
                },
            ],
            [
                'name' => 'created_at',
                'type' => 'date'
            ],
        ]);
    }
    
    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation() {
        CRUD::setValidation( ProductRequest::class );
        
        CRUD::addFields( [
            [
                'name'  => 'name',
                'type'  => 'text',
                'label' => 'Tên sản phẩm',
                'tab'   => 'Thông tin cơ bản',
            ],
            [
                'name'  => 'code',
                'label' => 'Mã sản phẩm',
                'type'  => 'text',
                'tab'   => 'Thông tin cơ bản',
            ],
            [
                'name'  => 'code',
                'label' => 'Mã sản phẩm',
                'type'  => 'text',
                'tab'   => 'Thông tin cơ bản',
            ],
            [
                'label'         => 'Nhóm sản phẩm',
                'type'          => 'relationship',
                'name'          => 'product_category_id',
                'entity'        => 'product_category',
                'attribute'     => 'name',
                'inline_create' => [
                    'modal_route'  => route( 'product_category-inline-create' ),
                    'create_route' => route( 'product_category-inline-create-save' ),
                ],
                'ajax'          => true,
                'tab'           => 'Thông tin cơ bản',
                'placeholder'   => 'Chọn hoặc thêm mới',
            ],
            [
                'name'  => 'detail',
                'type'  => 'ckeditor',
                'label' => 'Mô tả chi tiết',
                'tab'   => 'Chi tiết',
            ],[   // Table
                'name'            => 'properties',
                'label'           => 'Thông tin thêm',
                'type'            => 'table',
                'entity_singular' => 'thuộc tính', // used on the "Add X" button
                'columns'         => [
                    'name'  => 'Thuộc tính',
                    'desc'  => 'Giá trị',
                ],
                'max' => 10, // maximum rows allowed in the table
                'min' => 0, // minimum rows allowed in the table
                'tab'   => 'Chi tiết',
                'default' => [
                    ['name' => 'Kích thước','desc' => ''],
                    ['name' => 'Dung tích','desc' => ''],
                    ['name' => 'Trọng lượng','desc' => ''],
                    ['name' => 'Tình trạng kho','desc' => ''],
                    ['name' => 'Số lượng mua tối thiểu','desc' => ''],
                ],
            ],
            [
                'name'    => 'price',
                'label'   => 'Giá',
                'type'    => 'number',
                'default' => 0,
                'tab'     => 'Thông tin cơ bản',
                'prefix' => 'VNĐ',
            ],
            [   // Browse multiple
                'name'       => 'images',
                'label'      => 'Hình ảnh',
                'type'       => 'browse_multiple',
                'multiple'   => true, // enable/disable the multiple selection functionality
                'sortable'   => true, // enable/disable the reordering with drag&drop
                'mime_types' => [ 'image' ], // visible mime prefixes; ex. ['image'] or ['application/pdf']
                'tab'        => 'Thông tin cơ bản',
            ],
            [
                'label'         => 'Tags',
                'type'          => 'relationship',
                'name'          => 'tags', // the method that defines the relationship in your Model
                'entity'        => 'tags', // the method that defines the relationship in your Model
                'attribute'     => 'name', // foreign key attribute that is shown to user
                'pivot'         => true, // on create&update, do you need to add/delete pivot table entries?
                'inline_create' => [ 'entity' => 'tag' ],
                'ajax'          => true,
                'tab'           => 'Thông tin cơ bản',
            ],
        ] );
        
        CRUD::addField( [
            'name'        => 'seo_title',
            'type'        => 'text',
            'placeholder' => 'Seo title',
            'tab'         => 'SEO',
        ] );
        CRUD::addField( [
            'name'        => 'seo_description',
            'type'        => 'text',
            'placeholder' => 'Seo description',
            'tab'         => 'SEO',
        ] );
        
    }
    
    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation() {
        $this->setupCreateOperation();
    }
    
    protected function setupShowOperation(){
        CRUD::column( 'name')->type('text')->label('Tên sản phẩm');
        CRUD::column( 'code')->type('text')->label('Mã sản phẩm');
        CRUD::addColumn( [
            'name' => 'product_category_id',
            'type' => 'closure',
            'function' => function($entry){
                return $entry->product_category ? $entry->product_category->name : $entry->product_category_id;
            },
            'label' => 'Nhóm sản phẩm',
        ]);
        CRUD::column( 'images')->type('images')->label('Hình ảnh');
        CRUD::column( 'detail')->type('textarea')->label("Chi tiết");
        CRUD::addColumn([
            'name' => 'properties',
            'label' => 'Thông tin thêm',
            'type' => 'table',
            'columns'         => [
                'name'  => 'Thuộc tính',
                'desc'  => 'Giá trị',
            ],
        ] );
    }
    
    
    /**
     * Respond to AJAX calls from the select2 with entries from the Category model.
     * @return JSON
     */
    public function fetchProduct_category() {
        return $this->fetch( ProductCategory::class );
    }
    
    /**
     * Respond to AJAX calls from the select2 with entries from the Tag model.
     * @return JSON
     */
    public function fetchTags() {
        return $this->fetch( Tag::class );
    }
    
}
