<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProductCategoryRequest;
use App\Models\ProductCategory;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Str;

/**
 * Class ProductCategoryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ProductCategoryCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\InlineCreateOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\ProductCategory::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/product_category');
        CRUD::setEntityNameStrings('nhóm sản phẩm', 'nhóm sản phẩm');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumn([
            'name' => 'name',
            'type' => 'closure',
            'function' => function($entry){
                return "<a target='_blank' href='" . route( 'product_category', [ 'slug' => $entry->slug ]) . "'>" . Str::limit( $entry->name, 60) . "</a>";
            }
        ]);
        CRUD::addColumn('slug');
        CRUD::addColumn('parent');
        CRUD::addColumn([   // select_multiple: n-n relationship (with pivot table)
            'label'     => 'Product', // Table column heading
            'type'      => 'relationship_count',
            'name'      => 'products', // the method that defines the relationship in your Model
            'wrapper'   => [
                'href' => function ($crud, $column, $entry, $related_key) {
                    return backpack_url('product?product_category_id='.$entry->getKey());
                },
            ],
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ProductCategoryRequest::class);
    
        CRUD::addField([
            'name' => 'name',
            'label' => 'Name',
            'tab' => 'Thông tin cơ bản',
        ]);
        CRUD::addField([
            'name' => 'slug',
            'label' => 'Slug (URL)',
            'type' => 'text',
            'hint' => 'Will be automatically generated from your name, if left empty.',
            // 'disabled' => 'disabled',
            'tab' => 'Thông tin cơ bản',
        ]);
        CRUD::addField([
            'label' => 'Parent',
            'type' => 'select2_nested',
            'name' => 'parent_id',
            'entity' => 'parent',
            'attribute' => 'name',
            'placeholder' => 'Không',
            'tab' => 'Thông tin cơ bản',
            'model' => ProductCategory::class,
        ]);
        
        CRUD::addField( [
            'name' => 'introduction',
            'label' => 'Giới thiệu',
            'type' => 'ckeditor',
            'placeholder' => 'Giới thiệu ngắn về nhóm hàng',
            'tab' => 'Thông tin cơ bản',
            'options'       => [
                'autoGrow_minHeight'   => 200,
                'autoGrow_bottomSpace' => 50,
                'removePlugins'        => 'resize,maximize',
            ]
        ]);
        
        CRUD::addField( [
            'name' => 'seo_title',
            'type' => 'text',
            'placeholder' => 'Seo title',
            'tab' => 'SEO',
        ]);
        CRUD::addField( [
            'name' => 'seo_description',
            'type' => 'text',
            'placeholder' => 'Seo description',
            'tab' => 'SEO',
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
    
    protected function setupReorderOperation()
    {
        CRUD::set('reorder.label', 'name');
        CRUD::set('reorder.max_level', 2);
    }
}
