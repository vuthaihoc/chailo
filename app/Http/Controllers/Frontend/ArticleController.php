<?php
/**
 * Created by PhpStorm.
 * User: hocvt
 * Date: 2020-06-21
 * Time: 04:21
 */

namespace App\Http\Controllers\Frontend;


use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\ArticleCategory;

class ArticleController extends Controller {
    
    public function category($slug){
        $category = ArticleCategory::findBySlugOrFail( $slug );
        
        \SEO::setTitle( $category->seo_title ?: $category->name );
        \SEO::setDescription( $category->seo_description ?: $category->name );
        
        return view('frontend.article_category', compact( 'category'));
    }
    public function detail($slug){
        $article = Article::findBySlugOrFail( $slug );
        $category = $article->article_category;
    
        \SEO::setTitle( $article->seo_title ?: $article->name );
        \SEO::setDescription( $article->seo_description ?: $article->name . " - " . $category->name );
    
        return view('frontend.article_detail', compact( 'article', 'category'));
    }
    
}
