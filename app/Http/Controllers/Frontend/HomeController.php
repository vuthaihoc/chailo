<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        $new_products = Product::orderBy('id', 'desc')->take(12)->get();
        return view('frontend.home', compact( 'new_products'));
    }
}
