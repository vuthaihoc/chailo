<?php
/**
 * Created by PhpStorm.
 * User: hocvt
 * Date: 2020-06-21
 * Time: 04:21
 */

namespace App\Http\Controllers\Frontend;


use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductCategory;

class ProductController extends Controller {
    
    public function category($slug){
        
        $category = ProductCategory::findBySlugOrFail( $slug );
        
        \SEO::setTitle( $category->seo_title ?: $category->name );
        \SEO::setDescription( $category->seo_description ?: $category->name );
        
        return view('frontend.product_category', compact( 'category'));
    }
    public function detail($slug){
        $product = Product::findBySlugOrFail( $slug );
    
        \SEO::setTitle( $product->seo_title ?: $product->name );
        \SEO::setDescription( $product->seo_description ?: $product->name );
        
        $category = $product->product_category;
        return view('frontend.product_detail', compact( 'product', 'category'));
    }
    
}
