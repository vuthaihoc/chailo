<?php
/**
 * Created by PhpStorm.
 * User: hocvt
 * Date: 2020-06-21
 * Time: 02:41
 */

namespace App\Helpers;


use Illuminate\Support\Str;

class MenuHelper {
    
    public static function get($menu = 'menu_top') : array {
        $items = config('settings.' . $menu, "[]");
        $items = json_decode( $items, true );
        $result = [];
        foreach ($items as $item){
            $item['children'] = [];
            $item['url'] = isset( $item['url'] ) ? $item['url'] : "/";
            $item['title'] = isset( $item['title'] ) ? $item['title'] : $item['url'];
            $item['url'] = self::standardizeUrl( $item['url'] );
            if(count( $result ) > 0 && Str::startsWith( $item['title'], "-")){
                $item['title'] = ltrim( $item['title'], "- ");
                $result[count( $result ) - 1]['children'][] = $item;
            }else{
                $item['title'] = ltrim( $item['title'], "- ");
                $result[] = $item;
            }
        }
        return $result;
    }
    
    public static function standardizeUrl($url){
        if(strpos( $url, "//")){
            return $url;
        }else{
            return url($url);
        }
    }
    
}