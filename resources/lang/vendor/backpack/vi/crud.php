<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Backpack Crud Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the CRUD interface.
    | You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    // Forms
    'save_action_save_and_new'         => 'Save and new item',
    'save_action_save_and_edit'        => 'Save and edit this item',
    'save_action_save_and_back'        => 'Save and back',
    'save_action_save_and_preview'     => 'Save and preview',
    'save_action_changed_notification' => 'Default behaviour after saving has been changed.',

    // Create form
    'add'                 => 'Thêm',
    'back_to_all'         => 'Trở lại danh sách ',
    'cancel'              => 'Huỷ',
    'add_a_new'           => 'Thêm mới ',

    // Edit form
    'edit'                 => 'Sửa',
    'save'                 => 'Lưu',

    // Translatable models
    'edit_translations' => 'Dịch',
    'language'          => 'Ngôn ngữ',

    // CRUD table view
    'all'                       => 'Tất cả ',
    'in_the_database'           => 'trong CSDL',
    'list'                      => 'Danh sách',
    'reset'                     => 'Đặt lại',
    'actions'                   => 'Thao tác',
    'preview'                   => 'Xem trước',
    'delete'                    => 'Xoá',
    'admin'                     => 'Admin',
    'details_row'               => 'This is the details row. Modify as you please.',
    'details_row_loading_error' => 'There was an error loading the details. Please retry.',
    'clone' => 'Tạo bản sao',
    'clone_success' => '<strong>Entry cloned</strong><br>A new entry has been added, with the same information as this one.',
    'clone_failure' => '<strong>Cloning failed</strong><br>The new entry could not be created. Please try again.',

    // Confirmation messages and bubbles
    'delete_confirm'                              => 'Are you sure you want to delete this item?',
    'delete_confirmation_title'                   => 'Item Deleted',
    'delete_confirmation_message'                 => 'The item has been deleted successfully.',
    'delete_confirmation_not_title'               => 'NOT deleted',
    'delete_confirmation_not_message'             => "There's been an error. Your item might not have been deleted.",
    'delete_confirmation_not_deleted_title'       => 'Not deleted',
    'delete_confirmation_not_deleted_message'     => 'Nothing happened. Your item is safe.',

    // Bulk actions
    'bulk_no_entries_selected_title'   => 'Chưa chọn bản ghi nào',
    'bulk_no_entries_selected_message' => 'Please select one or more items to perform a bulk action on them.',

    // Bulk confirmation
    'bulk_delete_are_you_sure'   => 'Are you sure you want to delete these :number entries?',
    'bulk_delete_sucess_title'   => 'Các bản ghi đã được xoá',
    'bulk_delete_sucess_message' => ' items have been deleted',
    'bulk_delete_error_title'    => 'Lỗi khi xoá',
    'bulk_delete_error_message'  => 'Có bản ghi chưa xoá được',

    // Ajax errors
    'ajax_error_title' => 'Lôi',
    'ajax_error_text'  => 'Lỗi tải trang, hãy tải lại.',

    // DataTables translation
    'emptyTable'     => 'No data available in table',
    'info'           => 'Showing _START_ to _END_ of _TOTAL_ entries',
    'infoEmpty'      => 'No entries',
    'infoFiltered'   => '(filtered from _MAX_ total entries)',
    'infoPostFix'    => '.',
    'thousands'      => ',',
    'lengthMenu'     => '_MENU_ bản ghi mỗi trang',
    'loadingRecords' => 'Đang tải...',
    'processing'     => 'Đang xử lý...',
    'search'         => 'Tìm: ',
    'zeroRecords'    => 'Không có bản ghi phù hợp',
    'paginate'       => [
        'first'    => 'Đầu tiên',
        'last'     => 'Cuối cùng',
        'next'     => 'Sau',
        'previous' => 'Trước',
    ],
    'aria' => [
        'sortAscending'  => ': activate to sort column ascending',
        'sortDescending' => ': activate to sort column descending',
    ],
    'export' => [
        'export'            => 'Export',
        'copy'              => 'Copy',
        'excel'             => 'Excel',
        'csv'               => 'CSV',
        'pdf'               => 'PDF',
        'print'             => 'Print',
        'column_visibility' => 'Column visibility',
    ],

    // global crud - errors
    'unauthorized_access' => 'Unauthorized access - you do not have the necessary permissions to see this page.',
    'please_fix'          => 'Please fix the following errors:',

    // global crud - success / error notification bubbles
    'insert_success' => 'The item has been added successfully.',
    'update_success' => 'The item has been modified successfully.',

    // CRUD reorder view
    'reorder'                      => 'Reorder',
    'reorder_text'                 => 'Use drag&drop to reorder.',
    'reorder_success_title'        => 'Done',
    'reorder_success_message'      => 'Your order has been saved.',
    'reorder_error_title'          => 'Error',
    'reorder_error_message'        => 'Your order has not been saved.',

    // CRUD yes/no
    'yes' => 'Yes',
    'no'  => 'No',

    // CRUD filters navbar view
    'filters'        => 'Filters',
    'toggle_filters' => 'Toggle filters',
    'remove_filters' => 'Remove filters',

    // Fields
    'browse_uploads'            => 'Duyệt files',
    'select_all'                => 'Chọn tất cả',
    'select_files'              => 'Chọn files',
    'select_file'               => 'Chọn file',
    'clear'                     => 'Xoá trống',
    'page_link'                 => 'Page link',
    'page_link_placeholder'     => 'http://example.com/your-desired-page',
    'internal_link'             => 'Internal link',
    'internal_link_placeholder' => 'Internal slug. Ex: \'admin/page\' (no quotes) for \':url\'',
    'external_link'             => 'External link',
    'choose_file'               => 'Chọn file',
    'new_item'                  => 'Bản ghi mới',
    'select_entry'              => 'Select an entry',
    'select_entries'            => 'Select entries',

    //Table field
    'table_cant_add'    => 'Cannot add new :entity',
    'table_max_reached' => 'Maximum number of :max reached',

    // File manager
    'file_manager' => 'Quản lý files',

    // InlineCreateOperation
    'related_entry_created_success' => 'Related entry has been created and selected.',
    'related_entry_created_error' => 'Could not create related entry.',
];
