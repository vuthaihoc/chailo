{{-- image column type --}}
@php
  $value = data_get($entry, $column['name']);

  $column['height'] = $column['height'] ?? "100px";
  $column['width'] = $column['width'] ?? "auto";
  $column['radius'] = $column['radius'] ?? "100px";
  $column['prefix'] = $column['prefix'] ?? '';

@endphp

<span>
  @if( empty($value) || !is_array($value) )
    -
  @else
    @foreach($value as $v)
      @includeWhen(!empty($column['wrapper']), 'crud::columns.inc.wrapper_start')
      @php
        if (preg_match('/^data\:image\//', $v)) { // base64_image
        $href = $src = $v;
      } elseif (isset($column['disk'])) { // image from a different disk (like s3 bucket)
        $href = $src = Storage::disk($column['disk'])->url($column['prefix'].$v);
      } else { // plain-old image, from a local disk
        $href = $src = asset( $column['prefix'] . $v);
      }

      $column['wrapper']['element'] = $column['wrapper']['element'] ?? 'a';
      $column['wrapper']['href'] = $column['wrapper']['href'] ?? $href;
      $column['wrapper']['target'] = $column['wrapper']['target'] ?? '_blank';
      @endphp
      <img src="{{ $src }}" style="
        max-height: {{ $column['height'] }};
        width: {{ $column['width'] }};
        border-radius: {{ $column['radius'] }};"
      />
      @includeWhen(!empty($column['wrapper']), 'crud::columns.inc.wrapper_end')
    @endforeach
  @endif
</span>
