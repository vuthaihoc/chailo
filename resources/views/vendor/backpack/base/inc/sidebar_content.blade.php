<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>

<li class="nav-item nav-dropdown">
  <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-shopping-cart"></i> @lang('QL Sản phẩm')</a>
  <ul class="nav-dropdown-items pl-2">
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('product/create') }}'><i class='nav-icon la la-plus'></i> @lang('Thêm mới')</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('product') }}'><i class='nav-icon la la-list'></i> Danh Sách SP</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('product_category') }}'><i class='nav-icon la la-sitemap'></i> Nhóm SP </a></li>
  </ul>
</li>
<li class="nav-item nav-dropdown">
  <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-newspaper"></i> @lang('QL Bài viết')</a>
  <ul class="nav-dropdown-items pl-2">
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('article/create') }}'><i class='nav-icon la la-plus'></i> @lang('Thêm mới')</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('article') }}'><i class='nav-icon la la-file'></i> @lang('DS bài viết')</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('article_category') }}'><i class='nav-icon la la-sitemap'></i> @lang('Nhóm bài viết')</a></li>
  </ul>
</li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('tag') }}'><i class='nav-icon la la-tags'></i> Tags</a></li>

<!-- Users, Roles, Permissions -->
<li class="nav-item nav-dropdown">
  <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-users"></i> @lang('Người dùng')</a>
  <ul class="nav-dropdown-items pl-2">
    <li class="nav-item"><a class="nav-link" href="{{ backpack_url('user') }}"><i class="nav-icon la la-user"></i> <span>@lang('Danh sách')</span></a></li>
    <li class="nav-item"><a class="nav-link" href="{{ backpack_url('role') }}"><i class="nav-icon la la-id-badge"></i> <span>@lang('Nhóm')</span></a></li>
    <li class="nav-item"><a class="nav-link" href="{{ backpack_url('permission') }}"><i class="nav-icon la la-key"></i> <span>@lang('Quyền')</span></a></li>
  </ul>
</li>

<li class="nav-item nav-dropdown">
  <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-gear"></i> @lang('Hệ thống')</a>
  <ul class="nav-dropdown-items pl-2">
    <li class="nav-item"><a class="nav-link" href="{{ backpack_url('elfinder') }}\"><i class="nav-icon la la-files-o"></i> <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('setting') }}'><i class='nav-icon la la-cog'></i> <span>Settings</span></a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('log') }}'><i class='nav-icon la la-terminal'></i> @lang('Logs')</a></li>
  </ul>
</li>