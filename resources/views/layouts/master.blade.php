<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  {!! \SEO::generate() !!}

  <!-- Bootstrap core CSS -->
  <link href="{{url('css/app.css')}}" rel="stylesheet">
  <link href="{{url('font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
  <link href="{{url('owl/assets/owl.carousel.min.css')}}" rel="stylesheet">

  @yield("after_styles")

</head>

<body>

@include('partials.header')

<main role="main">

  @yield('content')

</main>

@include('partials.footer')

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="{{url('js/jquery-slim.min.js')}}" type="application/javascript"></script>
<script src="{{url('js/app.js')}}" type="application/javascript"></script>
<script src="{{url('owl/owl.carousel.min.js')}}" type="application/javascript"></script>
@yield('after_scripts')
@stack('after_scripts')
</body>
</html>
