<div class="card card-product-in-slide p-2 m-2">
  <img class="card-img-top" src="{{url('uploads/demo/chailo01.jpg')}}" alt="Card image cap">
  <div class="">
    <h3 class="card-text">
      <a href="{{route('product_detail', ['slug' => $product->slug])}}">{{$product->name}}</a>
    </h3>
    <a href="#" class="text-secondary font-bold"><b>{{$product->price ? $product->price_formatted : "Liên hệ"}}</b></a>
  </div>
</div>