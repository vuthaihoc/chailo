@php($home_slides = config('settings.home_slide') ? json_decode(config('settings.home_slide'), true) : [])
@if(count($home_slides))
<div class="container my-4">
  <div class="row">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        @foreach($home_slides as $k => $slide)
        <li data-target="#carouselExampleIndicators" data-slide-to="{{$k}}" class="{{$k == 0 ? "active" : ""}}"></li>
          @endforeach
      </ol>
      <div class="carousel-inner">
        @foreach($home_slides as $slide)
        <div class="carousel-item {{$loop->first ? "active" : ""}}">
          <img class="d-block w-100" src="{{url($slide['image'])}}" alt="{{$slide['name']}}">
        </div>
        @endforeach
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </div>
</div>
@endif