<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-6 mt-5">
        <p><b>{{config('settings.company_name')}}</b></p>
        <p class="text-muted">{{config('settings.company_address')}}
        <p>
        <p class="text-muted"><b>{{config('settings.company_contact')}}</b></p>
      </div>
      <div class="col-md-6 mt-5">
        <p>{{config('settings.company_address_1')}}
        <p>
        <p>{{config('settings.company_info')}}</p>
      </div>
    </div>
  </div>
  <div id="copy-right-bottom" class="mt-5">
    <div class="container py-2">
      &copy; Copyright Chailo2021
    </div>
  </div>
</footer>