<div class="row">
    <div class="card-sidebar mb-4">
        <div class="card-header">
            <i class="fa fa-bars"></i> DANH MỤC SẢN PHẨM
        </div>
        <div class="">
            <div class="list-group">
                @php($product_root_categories = \App\Models\ProductCategory::with('children')->whereNull('parent_id')->get())
                @foreach($product_root_categories as $category)
                    @if($category->children->count())
                        <div class="dropdown dropright">
                            <a href="{{route('product_category', ['slug' => $category->slug])}}"
                               class="dropdown-toggle list-group-item list-group-item-action bg-light"
                               style="margin-top: -1px;margin-bottom: -1px;"
                               data-toggle="dropdown"> <i class="fa fa-circle-o mr-2"></i> {{$category->name}}</a>
                            <div class="dropdown-menu w-100">
                                <div class="list-group list-group-flush">
                                    @foreach($category->children as $child_category)
                                        <a href="{{route('product_category', ['slug' => $child_category->slug])}}"
                                           class="list-group-item" href="#">{{$child_category->name}}</a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @else
                        <a href="{{route('product_category', ['slug' => $category->slug])}}"
                           class="list-group-item list-group-item-action bg-light"> <i
                                class="fa fa-circle-o mr-2"></i> {{$category->name}}</a>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="card-sidebar mb-4">
        <div class="card-header">
            <i class="fa fa-bars"></i> DANH MỤC SẢN PHẨM
        </div>
        <div class="">
            <div class="list-group list-group-flush">
                <a href="#" class="list-group-item list-group-item-action bg-light">Dashboard</a>
                <a href="#" class="list-group-item list-group-item-action bg-light">Shortcuts</a>
                <a href="#" class="list-group-item list-group-item-action bg-light">Overview</a>
                <a href="#" class="list-group-item list-group-item-action bg-light">Events</a>
                <a href="#" class="list-group-item list-group-item-action bg-light">Profile</a>
                <a href="#" class="list-group-item list-group-item-action bg-light">Status</a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="card-sidebar mb-4">
        <div class="card-header">
            <i class="fa fa-bars"></i> HỖ TRỢ TRỰC TUYẾN
        </div>
        <div class="p-3">
            <h4 class="mb-3">
                <i class="text-secondary fa fa-phone"></i> <b>Hotline tư vấn</b>
            </h4>
            <ul class="ml-0 pl-0">
                @foreach(json_decode(config('settings.support_hotlines', '[]'), true) as $support)
                    <li class="media my-2">
                        <span>{{$support['name']}}: </span>
                        <div class="media-body text-right">{{$support['phone']}}</div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
<div class="row">
    <div class="card-sidebar mb-4">
        <div class="card-header">
            <i class="fa fa-bars"></i> TIN HAY MỖI NGÀY
        </div>
        <div class="py-3">
            @php($latest_article = \App\Models\Article::latest()->first())
            @if($latest_article)
                <div class="card">
                    <img class="card-img-top"
                         src="{{$latest_article->image ? url($latest_article->image) : "data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22286%22%20height%3D%22180%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20286%20180%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_172c6b8cb9b%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A14pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_172c6b8cb9b%22%3E%3Crect%20width%3D%22286%22%20height%3D%22180%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22107.1875%22%20y%3D%2296.6%22%3E286x180%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E"}}"
                         alt="{{$latest_article->title}}">
                    <div class="card-body">
                        <h5 class="card-title">
                            <a href="{{route('article_detail', ['slug' => $latest_article->slug])}}">
                                {{$latest_article->title}}
                            </a>
                        </h5>
                        <p class="card-text">{!! \Illuminate\Support\Str::limit(strip_tags($latest_article->content), 150) !!}</p>
                        <a href="{{route('article_detail', ['slug' => $latest_article->slug])}}"
                           class="btn btn-primary">Đọc tiếp</a>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
