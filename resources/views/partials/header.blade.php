<div id="info_top">
    <div class="container">
        {{config('settings.welcome_strong_top')}}
    </div>
</div>
<div class="container" id="top">
    <div class="row">
        <div class="col-10 offset-1 offset-sm-0 col-sm-4 col-md-3 col-lg-3">
            <a href="{{url('/')}}">
                <img id="logo_top" src="{{url('logo.png')}}" class="align-middle"/>
            </a>
        </div>
        <div class="col-sm-8 col-md-9 col-lg-9 pl-lg-5 text-sm-right text-center">
            <form id="form_search_top">
                <input name="search" id="search_input_top" placeholder="Tìm kiếm ... "/>
                <button class="btn btn-search-top"> Tìm kiếm</button>
                <p>Top search : hũ PET, chai đựng dầu</p>
            </form>
        </div>
    </div>
</div>
<div class="bg-primary" id="menu_top">
    <nav class="navbar navbar-expand-md container">
        {{--<a class="navbar-brand" href="#">Navbar</a>--}}
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
                aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="fa fa-bars"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{url("/")}}"> <i class="fa fa-home"></i>Trang chủ </a>
                </li>
                @php($top_menu_items = \App\Helpers\MenuHelper::get('menu_top'))
                @foreach($top_menu_items as $item)
                    <li class="nav-item {{count($item['children']) ? "dropdown" : ""}}">
                        <a class="nav-link {!! count($item['children']) ? "dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false" : "" !!}"
                           href="{{$item['url']}}">{{$item['title']}}</a>
                        @if(count($item['children']))
                            <div class="dropdown-menu">
                                @foreach($item['children'] as $sub_item)
                                    <a class="dropdown-item" href="{{$sub_item['url']}}">{{$sub_item['title']}}</a>
                                @endforeach
                            </div>
                        @endif
                    </li>
                @endforeach
            </ul>
        </div>
        <div style="color: #fff;">
            {{config('settings.contact_email')}}
        </div>
    </nav>
</div>
