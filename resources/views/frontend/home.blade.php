@extends('layouts.master')



@section('content')

  @include('includes.home_slide')

  <div class="container">

    <div class="row">
      <div class="col-md-8 col-lg-9 order-md-2">
        <div class="pl-md-2">
          <div class="row">
            <div class="card-products-slide mb-4">
              <div class="card-header font-weight-bold">
                <i class="fa fa-shopping-cart"></i> SẢN PHẨM MỚI
                <div class="pull-right owl-slide-nav" id="owl-slide-nav-1"></div>
              </div>
              <div class="owl-carousel owl-theme" id="owl-carousel-1" data-nav-container="#owl-slide-nav-1">
                @foreach($new_products as $product)
                  <div class="item">
                    @include('includes.product-in-slide', compact('product'))
                  </div>
                @endforeach
              </div>
            </div>
            {{--<div class="card-products-slide mb-4">--}}
            {{--<div class="card-header font-weight-bold">--}}
            {{--<i class="fa fa-shopping-cart"></i> CHAI LỌ HŨ MỸ PHẨM CAO CẤP--}}
            {{--<div class="pull-right owl-slide-nav" id="owl-slide-nav-2" ></div>--}}
            {{--</div>--}}
            {{--<div class="owl-carousel owl-theme" id="owl-carousel-2" data-nav-container="#owl-slide-nav-2">--}}
            {{--<div class="item">--}}
            {{--@include('includes.product-in-slide')--}}
            {{--@include('includes.product-in-slide')--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<div class="card-products-slide mb-4">--}}
            {{--<div class="card-header font-weight-bold">--}}
            {{--<i class="fa fa-shopping-cart"></i> CHAI LỌ HŨ MỸ PHẨM CAO CẤP--}}
            {{--<div class="pull-right owl-slide-nav" id="owl-slide-nav-3" ></div>--}}
            {{--</div>--}}
            {{--<div class="owl-carousel owl-theme" id="owl-carousel-3" data-nav-container="#owl-slide-nav-3">--}}
            {{--<div class="item">--}}
            {{--@include('includes.product-in-slide')--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
          </div>
        </div>
      </div>
      <div class="col-md-4 col-lg-3 order-md-1">
        @include('partials.sidebar')
      </div>
    </div>

  </div>

@endsection


@push('after_scripts')
  <script>

      function applyOwl(slide_container, nav_container) {
          return $(slide_container).owlCarousel({
              loop: true,
              margin: 2,
              responsiveClass: true,
              responsive: {
                  0: {
                      items: 1,
                      nav: true
                  },
                  600: {
                      items: 3,
                      nav: false
                  },
                  1000: {
                      items: 4,
                      nav: true,
                      loop: false
                  }
              },
              navContainer: nav_container,
              dots: false
          });
      }

      $(document).ready(function () {
          applyOwl('#owl-carousel-1', '#owl-slide-nav-1');
          applyOwl('#owl-carousel-2', '#owl-slide-nav-2');
          applyOwl('#owl-carousel-3', '#owl-slide-nav-3');
      });

  </script>
@endpush