@extends('layouts.master')



@section('content')

    <section class="bread-crumb mt-3">
        <div class="container">
            <div class="row">
                <div class="w-100">
                    <ul class="breadcrumb list-unstyled " vocab="http://schema.org/" typeof="BreadcrumbList">
                        <li class="home mr-2" property="itemListElement" typeof="ListItem">
                            <a property="item" href="/" typeof="WebPage" title="Về trang chủ">
                                <span property="name">Trang chủ</span>
                            </a>
                            <meta property="position" content="1">
                        </li>
                        <li class="mr-2" property="itemListElement" typeof="ListItem">
                            <a property="item" href="{{route('article_category', ['slug' => $category->slug])}}"
                               title="{{$category->name}}" typeof="WebPage">
                                >> <span property="name">{{$category->name}}</span>
                            </a>
                            <meta property="position" content="2">
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <div class="container mt-2">

        <div class="row">
            <div class="col-md-8 col-lg-9 order-md-2">
                <div class="pl-md-2">
                    <div class="row">
                        <div class="category-intro p-1">
                            <h3 class="category-header">
                                Bài viết mục : {{$category->name}}
                            </h3>
                        </div>
                        <div class="card-products-slide mb-4">
                            @php($articles = $category->articles()->paginate(10))
                            <ul class="list-unstyled my-5 px-2">
                                @foreach($articles as $article)

                                    <li class="media py-2" style="height: 200px;border-bottom: 1px solid #cccccc;">
                                        <img class="mr-3" style="width: 200px;" src="{{url($article->image)}}"
                                             alt="{{$article->title}}">
                                        <div class="media-body">
                                            <h4 class="mt-0 mb-1">
                                                <a href="{{route('article_detail', ['slug' => $article->slug])}}">
                                                    {{$article->title}}
                                                </a></h4>
                                            <div class="text-muted py-2"><i class="fa fa-user"></i> Admin
                                                , {{$article->date}} </div>
                                            {!! \Illuminate\Support\Str::limit(strip_tags($article->content), 400) !!}
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-3 order-md-1">
                @include('partials.sidebar')
            </div>
        </div>

    </div>

@endsection


@push('after_scripts')
    <script>

      function applyOwl(slide_container, nav_container) {
        return $(slide_container).owlCarousel({
          loop: true,
          margin: 10,
          responsiveClass: true,
          responsive: {
            0: {
              items: 1,
              nav: true
            },
            600: {
              items: 3,
              nav: false
            },
            1000: {
              items: 4,
              nav: true,
              loop: false
            }
          },
          navContainer: nav_container,
          dots: false
        });
      }

      // $(document).ready(function(){
      //     applyOwl('#owl-carousel-1', '#owl-slide-nav-1');
      //     applyOwl('#owl-carousel-2', '#owl-slide-nav-2');
      //     applyOwl('#owl-carousel-3', '#owl-slide-nav-3');
      // });

    </script>
@endpush
