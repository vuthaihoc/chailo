@extends('layouts.master')



@section('content')

  <section class="bread-crumb mt-3">
    <div class="container">
      <div class="row">
        <div class="w-100">
          <ul class="breadcrumb list-unstyled " vocab="http://schema.org/" typeof="BreadcrumbList">
            <li class="home mr-2" property="itemListElement" typeof="ListItem">
              <a property="item" href="/" typeof="WebPage" title="Về trang chủ">
                <span property="name">Trang chủ</span>
              </a>
              <meta property="position" content="1">
            </li>
            <li class="mr-2" property="itemListElement" typeof="ListItem">
              <a property="item" href="{{route('product_category', ['slug' => $category->slug])}}" title="{{$category->name}}" typeof="WebPage">
                >> <span property="name">{{$category->name}}</span>
              </a>
              <meta property="position" content="2">
            </li>
          </ul>
        </div>
      </div>
    </div>
  </section>

  <div class="container mt-2">

    <div class="row">
      <div class="col-md-8 col-lg-9 order-md-2">
        <div class="pl-md-2">
          <div class="row">
            <div class="category-intro p-1">
              <h3 class="category-header">
                {{$category->seo_title ?? $category->name}}
              </h3>
              <div class="category-description">
                <p>
                  {!! $category->introduction !!}
                </p>
              </div>
            </div>
            <div class="card-products-slide mb-4">
              <div class="card-header font-weight-bold">
                <i class="fa fa-shopping-cart"></i> {{$category->name}}
              </div>
              <div class="">
                <div class="row no-gutters">
                  @foreach($category->products as $product)
                  <div class="col-xl-3 col-lg-4 col-md-6">
                    @include('includes.product-in-slide', compact('product'))
                  </div>
                  @endforeach
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4 col-lg-3 order-md-1">
        @include('partials.sidebar')
      </div>
    </div>

  </div>

@endsection


@push('after_scripts')
  <script>

      function applyOwl(slide_container, nav_container){
          return $(slide_container).owlCarousel({
              loop:true,
              margin:10,
              responsiveClass:true,
              responsive:{
                  0:{
                      items:1,
                      nav:true
                  },
                  600:{
                      items:3,
                      nav:false
                  },
                  1000:{
                      items:4,
                      nav:true,
                      loop:false
                  }
              },
              navContainer: nav_container,
              dots: false
          });
      }

      // $(document).ready(function(){
      //     applyOwl('#owl-carousel-1', '#owl-slide-nav-1');
      //     applyOwl('#owl-carousel-2', '#owl-slide-nav-2');
      //     applyOwl('#owl-carousel-3', '#owl-slide-nav-3');
      // });

  </script>
@endpush