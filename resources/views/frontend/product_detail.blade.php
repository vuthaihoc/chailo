@extends('layouts.master')



@section('content')
  <section class="bread-crumb mt-3">
    <div class="container">
      <div class="row">
        <div class="w-100">
          <ul class="breadcrumb list-unstyled " vocab="http://schema.org/" typeof="BreadcrumbList">
            <li class="home mr-2" property="itemListElement" typeof="ListItem">
              <a property="item" href="/" typeof="WebPage" title="Về trang chủ">
                <span property="name">Trang chủ</span>
              </a>
              <meta property="position" content="1">
            </li>
            <li class="mr-2" property="itemListElement" typeof="ListItem">
              <a property="item" href="{{route('product_category', ['slug' => $category->slug])}}" title="{{$category->name}}" typeof="WebPage">
                >> <span property="name">{{$category->name}}</span>
              </a>
              <meta property="position" content="2">
            </li>
            <li>
              >> <span property="name">{{$product->name}}</span>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <div class="container mt-2">

    <div class="row">
      <div class="col-md-8 col-lg-9 order-md-2">
        <div class="pl-md-2">
          <div class="row">
            <div class="product-detail col-md-6">
              <h3 class="mb-5">
                <a href="#">
                  {{$product->name}}
                </a>
              </h3>
              <p>
                <b>Thông tin sản phẩm : </b>
              </p>
              <div class="ml-2">
                <p>
                  + Mã sản phẩm : {{$product->code}}
                </p>
                @foreach($product->properties as $property)
                  @if(!empty($property['desc']) && !empty($property['name']))
                    <p>
                      + {{$property['name']}} : {{$property['desc']}}
                    </p>
                  @endif
                @endforeach
              </div>


              <p class="text-secondary mt-5" style="font-size: 150%;">
                Giá : <b>{{$product->price ? $product->price_formatted . " VNĐ" : "Liên hệ"}}</b>
              </p>
              <p>
                <b>Tags : </b><br/>
                @foreach($product->tags as $tag)
                  <button type="button" class="btn btn-outline-primary">{{$tag->name}}</button>
                @endforeach
              </p>

            </div>
            <div class="product-images col-md-6">
              <div style="min-height: 300px;width: 100%;">
                <div class="owl-carousel owl-theme" id="product-detail-images-slide">
                  @foreach($product->images as $image)
                    <img src="{{url($image)}}"/>
                  @endforeach
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="card-products-slide mb-4 mt-5">
              <div class="card-header font-weight-bold">
                <i class="fa fa-shopping-cart"></i> Mô tả chi tiết
              </div>
              <div class="col-12 my-5 product-detail-container rft-wrapper">
                {!! $product->detail !!}
              </div>
            </div>
          </div>
          <div class="row">
            <div class="card-products-slide mb-4">
              <div class="card-header font-weight-bold">
                <i class="fa fa-shopping-cart"></i> Sản phẩm liên quan
              </div>
              <div class="">
                <div class="row no-gutters">
                  @foreach([1,2,3,4,5,6,7,8] as $i)
                    <div class="col-xl-3 col-lg-4 col-md-6">
                      @include('includes.product-in-slide')
                    </div>
                  @endforeach
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4 col-lg-3 order-md-1">
        @include('partials.sidebar')
      </div>
    </div>

  </div>

@endsection


@push('after_scripts')
  <script>

      $('#product-detail-images-slide').owlCarousel({
          loop: true,
          margin: 10,
          nav: false,
          dots: true,
          items: 1
      })

  </script>
@endpush
