<?php

use Illuminate\Database\Seeder;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if ( \App\Models\Article::count() ) {
            return;
        }
        foreach (
            [
                [
                    'name'     => 'Giới thiệu',
                    'children' => [
                        [
                            'name' => 'Giới thiệu về công ty',
                        ],
                        [
                            'name' => 'Hình thức thanh toán',
                        ],
                        [
                            'name' => 'Vận chuyển và giao nhận',
                        ]
                    ],
                ],
                [
                    'name' => 'Hoạt động công ty',
                ],
                [
                    'name'     => 'Tin tức',
                    'children' => [
                        [
                            'name' => 'Bao bì thuỷ tinh',
                        ],
                        [
                            'name' => 'Thông tin tuyển dụng',
                        ],
                    
                    ],
                ],
            ] as $info
        ) {
            $this->createCat( $info );
        }
    }
    
    protected function createCat( $info ) {
        $cat = \App\Models\Article::create( \Illuminate\Support\Arr::only( $info, [
            'name',
            'parent_id',
        ] ) );
        if ( isset( $info['children'] ) ) {
            foreach ( $info['children'] as $i ) {
                $i['parent_id'] = $cat->id;
                $this->createCat( $i );
            }
        }
        
    }
}
