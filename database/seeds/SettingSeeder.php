<?php

use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * The settings to add.
     */
    protected $settings = [
        [
            'key'         => 'welcome_strong_top',
            'name'        => 'Câu chào đầu trang',
            'description' => 'Câu chào hiển thị đầu trang web',
            'value'       => 'Chào mừng bạn đến với Chai lọ nhựa',
            'field'       => '{"name":"value","label":"Giá trị","type":"text"}',
            'active'      => 1,
        ],
        [
            'key'           => 'contact_email',
            'name'          => 'Email liên hệ',
            'description'   => 'Email liên hệ.',
            'value'         => 'kinhdoanh@chailo.vn',
            'field'         => '{"name":"value","label":"Value","type":"email"}',
            'active'        => 1,
        
        ],
        [
            'key'           => 'company_name',
            'name'          => 'Tên công ty',
            'description'   => 'Tên công ty.',
            'value'         => 'CÔNG TY TNHH BAO BÌ PAVICO',
            'field'         => '{"name":"value","label":"Value","type":"text"}',
            'active'        => 1,
        ],
        [
            'key'           => 'company_address',
            'name'          => 'Địa chỉ công ty',
            'description'   => 'Địa chỉ công ty.',
            'value'         => 'Số 04 Lô TT03, KĐT HD Mon, Ngõ 2 Hàm Nghi, Nam Từ Liêm, Hà Nội',
            'field'         => '{"name":"value","label":"Value","type":"text"}',
            'active'        => 1,
        ],
        [
            'key'           => 'company_address_1',
            'name'          => 'Địa chỉ công ty phụ',
            'description'   => 'Địa chỉ công ty phụ.',
            'value'         => 'Địa chỉ ĐKKD: Số 3, NV 31 Khu đô thị mới Lideco Bắc Quốc Lộ 32, Thị trấn Trạm Trôi, Hoài Đức, Hà Nội',
            'field'         => '{"name":"value","label":"Value","type":"text"}',
            'active'        => 1,
        ],
        [
            'key'           => 'company_contact',
            'name'          => 'Thông tin liên hệ công ty',
            'description'   => 'Thông tin liên hệ cuối trang.',
            'value'         => 'Điện thoại: 0981.199.772 - 0981.449.066',
            'field'         => '{"name":"value","label":"Value","type":"text"}',
            'active'        => 1,
        ],
        [
            'key'           => 'company_info',
            'name'          => 'Thông tin khác',
            'description'   => 'Thông tin khác cuối trang.',
            'value'         => 'GPKD: 0107540600, cấp ngày 17/08/2016 - Nơi cấp: sở KH & ĐT TP Hà Nội',
            'field'         => '{"name":"value","label":"Value","type":"text"}',
            'active'        => 1,
        ],
        
    ];
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    
//        $this->settings[] = [
//            'key'           => 'home_feature_categories',
//            'name'          => 'Nhóm sản phẩm nổi bật',
//            'description'   => 'Nhóm sản phẩm hiển thị trang chủ.',
//            'value'         => '',
//            'field'         => json_encode( [
//                'name' => 'value',
//                'label' => 'Value',
//                'type' => 'select2_from_ajax_multiple',
////                'entity' => 'product_categories',
//                'data_source' => url( 'admin/product/fetch/product_category'),
//                'pivot' => false,
//
//                'model' => \App\Models\ProductCategory::class,
//                'placeholder' => 'Nhóm sản phẩm',
//                'minimum_input_length' => 2,
//                'attribute' => 'name',
//                'method' => 'post',
//
//            ]),
//            'active'        => 1,
//        ];
        $this->settings[] = [
            'key'           => 'home_slide',
            'name'          => 'Slide ảnh trang chủ',
            'description'   => 'Chọn ảnh và link',
            'value'         => '[{"image":"uploads/demo/slider_1_image.png","link":"","name":"Slide 1"},{"image":"uploads/demo/slider_2_image.png","link":"","name":""},{"image":"uploads/demo/slider_3_image.png","link":"","name":""}]',
            'field'         => json_encode( [
                'name' => 'value',
                'label' => 'Value',
                'type' => 'repeatable',
                'fields' => [
                    [   // Browse
                        'name'  => 'image',
                        'label' => 'Image',
                        'type'  => 'browse',
                        'wrapper' => ['class' => 'form-group col-md-4'],
                    ],
                    [
                        'name'    => 'link',
                        'type'    => 'url',
                        'label'   => 'link',
                        'wrapper' => ['class' => 'form-group col-md-4'],
                    ],
                    [
                        'name'    => 'name',
                        'type'    => 'text',
                        'label'   => 'Alt text',
                        'wrapper' => ['class' => 'form-group col-md-4'],
                    ],
                ]
                
            ]),
            'active'        => 1,
        ];
        $this->settings[] = [
            'key'           => 'menu_top',
            'name'          => 'Menu top',
            'description'   => 'Menu top',
            'value'         => '[{"title":"Giới thiệu","url":"/"},{"title":"Hũ nhựa PET","url":"/"},{"title":"Vỏ chai"},{"title":"- Vỏ chai tinh dầu"},{"title":"- Vỏ chai nắp xoáy"}]',
            'field'         => json_encode( [
                'name' => 'value',
                'label' => 'Value',
                'type' => 'table',
                'entity_singular' => 'Thêm',
                'columns' => [
                    'title' => 'Link text',
                    'url' => 'Đường dẫn',
                ],
                'hint' => 'Thêm dấu trừ trước tên để nó là mục con của menu trước đó không có dấu trừ',
                
            ]),
            'active'        => 1,
        ];
        $this->settings[] = [
            'key'           => 'support_hotlines',
            'name'          => 'Tư vấn hỗ trợ',
            'description'   => 'Tên/SĐT tư vấn',
            'value'         => '[{"name":"Ms. Hà","phone":"0988456789"},{"name":"Ms. Nhung","phone":"0988456789"},{"name":"Mr. Thành","phone":"0988456789"}]',
            'field'         => json_encode( [
                'name' => 'value',
                'label' => 'Value',
                'type' => 'table',
                'entity_singular' => 'Thêm',
                'columns' => [
                    'name' => 'Tên',
                    'phone' => 'Số điện thoại',
                ],
                'hint' => '',
                
            ]),
            'active'        => 1,
        ];
        
//        DB::table('settings')->truncate();
        foreach ($this->settings as $index => $setting) {
            if(DB::table('settings')->where( 'key', $setting['key'])->exists()){
                continue;
            }
            $result = DB::table('settings')->insert($setting);
            
            if (!$result) {
                $this->command->info("Insert failed at record $index.");
                
                return;
            }
        }
        
        $this->command->info('Inserted '.count($this->settings).' records.');
    }
}
