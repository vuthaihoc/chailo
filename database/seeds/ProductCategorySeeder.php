<?php

use App\Models\ProductCategory;
use Illuminate\Database\Seeder;

class ProductCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if ( ProductCategory::count() ) {
            return;
        }
        foreach (
            [
                [
                    'name'     => 'Vỏ chai tinh dầu',
                ],
                [
                    'name'     => 'Bao bì nhựa',
                    'children' => [
                        [
                            'name' => 'Chai nhựa PET',
                        ],
                        [
                            'name' => 'Hũ nhựa PET',
                        ],
                        [
                            'name' => 'Hũ nhựa đựng thực phẩm',
                        ]
                    ],
                ],
                [
                    'name'     => 'Bao bì mỹ phẩm',
                    'children' => [
                        [
                            'name' => 'Vỏ son môi',
                        ],
                        [
                            'name' => 'Chai lọ đựng mỹ phẩm',
                        ],
                        [
                            'name' => 'Vỏ chai nước hoa',
                        ]
                    ],
                ],
                [
                    'name'     => 'Chai lọ dược phẩm',
                ],
                [
                    'name'     => 'Phụ kiện, nắp nút',
                ],
            ] as $info
        ) {
            $this->createCat( $info );
        }
    }
    
    protected function createCat( $info ) {
        $cat = ProductCategory::create( \Illuminate\Support\Arr::only( $info, [
            'name',
            'parent_id',
        ] ) );
        if ( isset( $info['children'] ) ) {
            foreach ( $info['children'] as $i ) {
                $i['parent_id'] = $cat->id;
                $this->createCat( $i );
            }
        }
        
        return $cat;
    }
}
