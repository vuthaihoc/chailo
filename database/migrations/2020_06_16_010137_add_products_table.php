<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('product_category_id');
            $table->string( "code")->comment( "Mã sản phẩm")->index()->nullable();
            $table->string('name');
            $table->string('seo_title')->nullable();
            $table->string('seo_description')->nullable();
            $table->string('slug')->default('');
            
            $table->json( 'properties')->comment( 'Các thuộc tính')->nullable();
            $table->text('detail')->comment( "Mo ta chi tiet")->nullable();
            $table->text('images')->nullable()->comment( "Các ảnh sản phẩm");
    
            $table->integer('price')->default(0);
    
            $table->enum('status', ['PUBLISHED', 'DRAFT'])->default('PUBLISHED');
            $table->boolean('featured')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('product_categories', function (Blueprint $table) {
            $table->id();
            $table->string('seo_title')->nullable();
            $table->string('seo_description')->nullable();
            $table->text( 'introduction')->nullable();
            $table->unsignedBigInteger('parent_id')->default(0)->nullable();
            $table->integer('lft')->unsigned()->nullable();
            $table->integer('rgt')->unsigned()->nullable();
            $table->integer('depth')->unsigned()->nullable();
            $table->string('name');
            $table->string('slug')->unique()->default('');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('product_tag', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('tag_id');
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
        Schema::dropIfExists('product_categories');
        Schema::dropIfExists('product_tag');
    }
}
