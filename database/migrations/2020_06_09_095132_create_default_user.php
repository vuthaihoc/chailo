<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDefaultUser extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        if ( config( 'app.env' ) == 'local' ) {
            \App\User::firstOrCreate( [
                'email' => 'admin@admin.com',
            ],
                [
                    'name'     => 'Administrator',
                    'password' => bcrypt( 'password' ),
                ] );
        }
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table( 'users', function ( Blueprint $table ) {
            //
        } );
    }
}
